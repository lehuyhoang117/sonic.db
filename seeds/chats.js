// const xid = require("xid-js")
// var randomProfile = require('random-profile-generator');
// var Chance = require("chance"),
//     chance = new Chance()
// const rd = require("random-names-places")
// const randomName = uniqueNamesGenerator({ dictionaries: [adjectives, colors, animals] }) // bigreddonkey
const { date, random, lorem } = require("faker")
// var goby = require("goby").init()
// var moniker = require("moniker")
// const { uniqueNamesGenerator, adjectives, colors, animals } = require("unique-names-generator")
// const cfg = { dictionaries: [adjectives, animals, colors], separator: " ", length: 3 }
// var names = moniker.generator([moniker.adjective, moniker.noun, moniker.verb], { glue: " " })

exports.seed = function (knex) {
    return
    console.log("Generating chats...")

    let count = 0
    let kids = []

    mom = Array.from({ length: 1000 }).map(() => {
        count++
        let chanid = random.number({ min: 1101, max: 2000 })

        let reps = Array.from({ length: 10 }).map(() => {
            let txt = lorem.text(),
                re = count,
                nor = 0,
                by = random.number({ min: 1, max: 1000 }),
                timestamp = date.recent(365)
            return {
                txt,
                re,
                nor,
                chanid,
                by,
                timestamp,
            }
        })

        kids = kids.concat(reps)
        
        let txt = lorem.text(),
            re = 0,
            nor = reps.length,
            by = random.number({ min: 1, max: 1000 }),
            timestamp = date.recent(365)

        return {
            txt,
            re,
            nor,
            chanid,
            by,
            timestamp,
        }
    })

    // return knex("chats")
    //     .del()
    //     .then(async function () {
    //         await knex("chats").insert(mom)
    //         await knex("chats").insert(kids)
    //     })
}
