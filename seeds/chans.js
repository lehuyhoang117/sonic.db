// const xid = require("xid-js")
// var randomProfile = require("random-profile-generator")
// var Chance = require("chance")
//     chance = new Chance()
const { date, random, lorem, internet } = require("faker")
const rd = require("random-names-places")
var goby = require("goby").init()
var moniker = require("moniker")
var monikers = moniker.generator([moniker.adjective, moniker.noun, moniker.verb], { glue: " " })

exports.seed = async function (knex) {
    return
    await knex("chans").del()
    await knex("chats").del()
    await knex("chanchats").del()

    console.log("Generating chans...")

    let grandmoms_len = 10
    let moms_len = 100
    let kids_len = 10

    const names = new Set()
    grandmoms = Array.from({ length: grandmoms_len }).map(() => {
        mom = 0
        by = random.number({ min: 1, max: 1000 })
        fullname = rd.place()
        timestamp = date.recent(365)
        let name = fullname.toLowerCase().replace(/ /g, "_")

        while (names.has(name)) {
            fullname = rd.place()
            name = fullname.toLowerCase()
        }
        names.add(name)

        return {
            fullname,
            name,
            mom,
            by,
            dir:[],
            ispub: false,
            isprv: true,
            timestamp,
        }
    })

    grandmoms = await knex.batchInsert("chans", grandmoms, 2000).returning(["id", "mom"])
    console.log(grandmoms.length)

    let moms = Array.from({ length: moms_len }).map(() => {
        mom = random.number({ min: 1, max: grandmoms.length })
        by = random.number({ min: 1, max: 10000 })
        fullname = goby.generate(["adj", "suf"]).toLowerCase()
        timestamp = date.recent(365)
        let name = fullname.toLowerCase().replace(/ /g, "_")
        dir = [mom]

        while (names.has(name)) {
            fullname = goby.generate(["adj", "suf"]).toLowerCase()
            name = fullname.toLowerCase()
        }
        names.add(name)

        return {
            fullname,
            name,
            mom,
            by,
            dir,
            ispub: false,
            isprv: false,
            timestamp,
        }
    })

    moms = await knex.batchInsert("chans", moms, 2000).returning(["id", "dir"])
    console.log(moms.length)

    let kids = await moms.reduce(async (kids, mom) => {
        await genchats(knex, mom.id, mom.dir)

        let n = kids_len //random.number({ min: 10, max: 20 })
        let k = Array.from({ length: n }).map(() => {
            by = random.number({ min: 1, max: 10000 })
            fullname = goby.generate(["pre", "adj", "suf"])
            let name = fullname.toLowerCase().replace(/ /g, "_")
            fullname = fullname + " in " + monikers.choose()
            timestamp = date.recent(365)
            dir = [...mom.dir, mom.id]

            while (names.has(name)) {
                fullname = goby.generate(["pre", "adj", "suf"])
                name = fullname.toLowerCase().replace(/ /g, "_")
                fullname = fullname + " in " + monikers.choose()
            }
            names.add(name)

            return {
                fullname,
                name,
                mom: mom.id,
                by,
                dir,
                ispub: false,
                isprv: false,
                timestamp,
            }
        })
        return (await kids).concat(k)
    }, [])

    kids = await knex.batchInsert("chans", kids, 2000).returning(["id", "dir"])
    console.log("kids.length", kids.length)

    await kids.reduce(async (_, kid) => {
        await genchats(knex, kid.id, kid.dir)
    })
}

async function genchats(knex, chanid, chandir) {
    let length = 10
    let nor = 5

    let moms = []
    let re = 0

    let path = chandir.concat([chanid])

    for (let index = 0; index < length; index++) {
        txt = lorem.text()
        by = random.number({ min: 1, max: 10000 })
        timestamp = date.recent(365)
        let mom = {
            txt,
            re,
            nor,
            by,
            userid: by,
            chanid,
            dir: path,
            timestamp,
        }
        ids = await knex('chats').insert(mom, "id")
        re = ids[0]
        moms.push(re)
    }
    // console.log(moms.length)

    let kids = []
    let chanchats = []

    moms.map(re => {
        path.map(chanid => {
            chanchats.push({
                chanid,
                chatid: re,
            })
        })

        let k = Array.from({ length: nor }).map(() => {
            txt = lorem.text()
            by = random.number({ min: 1, max: 10000 })
            timestamp = date.recent(365)
            return {
                txt,
                re,
                nor: 0,
                by,
                userid: by,
                chanid,
                dir: path,
                timestamp,
            }
        })
        kids = kids.concat(k)
    })

    kids = await knex.batchInsert("chats", kids, 2000).returning("id")

    kids.map(chatid => {
        path.map(chanid => {
            chanchats.push({
                chanid,
                chatid,
            })
        })
    })

    chanchats = await knex.batchInsert("chanchats", chanchats, 2000).returning("chanid")
}
