const { date, random } = require("faker")

exports.seed = function (knex) {
    return
    console.log("Generating userchans...")

    const ids = new Set()

    userchans = Array.from({ length: 20000 }).map(() => {
        let by = random.number({ min: 1, max: 1000 })
        let chanid = random.number({ min: 1, max: 1100 })
        const timestamp = date.recent(365)

        let id = chanid + "-" + by

        while (ids.has(id)) {
            by = random.number({ min: 1, max: 1000 })
            chanid = random.number({ min: 1, max: 1100 })
            id = chanid + "-" + by
        }
        ids.add(id)

        return {
            chanid,
            by,
            timestamp,
        }
    })

    return knex("userchans")
        .del()
        .then(function () {
            return knex("userchans").insert(userchans)
        })
}
