const { date, random } = require("faker")

exports.seed = function (knex) {
    return
    console.log("Generating chanchats...")

    const ids = new Set()

    chanchats = Array.from({ length: 20000 }).map(() => {
        let chanid = random.number({ min: 1, max: 1100 })
        let chatid = random.number({ min: 1, max: 11000 })
        const timestamp = date.recent(365)

        let id = chanid + "-" + chatid

        while (ids.has(id)) {
            chanid = random.number({ min: 1, max: 1100 })
            chatid = random.number({ min: 1, max: 1000 })
            id = chanid + "-" + chatid
        }
        ids.add(id)

        return {
            chanid,
            chatid,
            timestamp,
        }
    })

    return knex("chanchats")
        .del()
        .then(function () {
            return knex("chanchats").insert(chanchats)
        })
}
