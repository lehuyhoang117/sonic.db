const xid = require("xid-js")
// var randomProfile = require('random-profile-generator');
// var Chance = require('chance'), chance = new Chance();
const { name: fkname, date, image, internet, random } = require("faker")

exports.seed = async function (knex) {
    // await knex("users").del()
    // await knex("contacts").del()

    console.log("Generating users...")

    const names = new Set()
    let contacts = []
    let i = 0
    users = Array.from({ length: 10000 }).map(() => {
        i++
        const gender = random.arrayElement(["male", "female"])
        // const fullname = name.findName(gender);
        const firstName = fkname.firstName(gender)
        const lastName = fkname.lastName(gender)
        const timestamp = date.recent(365)
        let name = firstName.toLowerCase()
        while (names.has(name)) {
            name = `${firstName.toLowerCase()}${random.number(1000)}`
        }
        names.add(name)
        
        const email = internet.email(name).toLowerCase()

        contacts.push({
            name: `${firstName} ${lastName}`,
            email,
            by: i,
            timestamp,
        })

        return {
            name,
            email,
            fullname: `${firstName} ${lastName}`,
            key: xid.next(),
            timestamp,
            // picture: image.avatar(),
            // first_name: firstName,
            // last_name: lastName,
            // gender: gender === "male",
            // last_login: Math.random() > 0.5 ? date.between(timestamp, new Date()) : null,
            // updated_at: timestamp,
        }
    })

    us = await knex.batchInsert("users", users, 2000)//.returning(["id", "name"])
    // cs = await knex.batchInsert("contacts", contacts, 2000)//.returning(["id", "name"])

    // return knex("users")
    //     .del()
    //     .then(function () {
    //         return knex("users").insert(users)
    //     })
}
