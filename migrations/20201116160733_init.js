exports.up = function (knex) {
    return knex.schema
        .createTable("chans", table => {
            table.increments("id").unsigned().notNullable().primary()
            table.string("fullname", 150).notNullable()
            table.integer("mom").unsigned().notNullable()
            table.integer("by").unsigned().notNullable()
            table.specificType("to", "integer ARRAY").unsigned()
            table.specificType("dir", "integer ARRAY").unsigned().notNullable()
            table.specificType("md", "smallint").unsigned().notNullable().defaultTo(0)
            table.specificType("status", "smallint").unsigned().notNullable().defaultTo(0)
            table.specificType('tsv', 'tsvector').notNullable()
            table.integer("chatid").unsigned().notNullable().defaultTo(0)
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.text("about")
            table.integer("re").unsigned()
            table.index("mom")
            table.index("by")
            table.index("to")
            table.index("dir")
            table.index("md")
            table.index("status")
            table.index("chatid")
        })
        .raw("CREATE INDEX chans_tsv_index ON chans USING GIN(tsv)")
        .raw(`CREATE TRIGGER tsvectorupdatechans
            BEFORE INSERT OR UPDATE
            ON chans
            FOR EACH ROW
            EXECUTE PROCEDURE tsvector_update_trigger(tsv, 'pg_catalog.english', fullname)`)
        .createTable("chats", table => {
            table.increments("id").unsigned().notNullable().primary()
            table.text("txt").notNullable()
            table.integer("by").unsigned().notNullable()
            table.integer("chanid").unsigned().notNullable()
            table.specificType("dir", "integer ARRAY").unsigned().notNullable()
            table.specificType("status", "smallint").unsigned().notNullable().defaultTo(0)
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.specificType("men", "integer ARRAY").unsigned()
            table.specificType("stars", "integer ARRAY").unsigned()
            table.integer("re").unsigned()
            table.integer("nor").unsigned().notNullable().defaultTo(0)
            table.specificType('tsv', 'tsvector').notNullable()
            table.index("chanid")
            table.index("re")
            table.index("by")
            table.index("dir")
            table.index("men")
            table.index("stars")
            table.index("status")
        })
        .raw("CREATE INDEX chats_tsv_index ON chats USING GIN(tsv)")
        .raw(`CREATE TRIGGER tsvectorupdatechats
            BEFORE INSERT OR UPDATE
            ON chats
            FOR EACH ROW
            EXECUTE PROCEDURE tsvector_update_trigger(tsv, 'pg_catalog.english', txt)`)
        .raw(`CREATE OR REPLACE FUNCTION log_chans_chatid() RETURNS TRIGGER AS $$
            BEGIN
                UPDATE chans SET chatid = NEW.id WHERE id = ANY(NEW.dir);
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;`)
        .raw(`CREATE TRIGGER after_chats_insert_log_chans_chatid
            AFTER INSERT
            ON chats
            FOR EACH ROW
            EXECUTE PROCEDURE log_chans_chatid();`)
        .createTable("userchans", table => {
            table.primary(["by", "chanid"])
            table.integer("by").unsigned().notNullable()
            table.integer("chanid").unsigned().notNullable()
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.index("by")
            table.index("chanid")
            // table.index("timestamp")
        })
        .createTable("chatlogs", table => {
            table.increments("id").unsigned().notNullable().primary()
            table.specificType("action", "smallint").unsigned().notNullable()
            table.integer("chatid").unsigned().notNullable()
            table.integer("by").unsigned().notNullable()
            table.specificType("dir", "integer ARRAY").unsigned().notNullable()
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.index("by")
            table.index("chatid")
            table.index("dir")
        })
        .createTable("chanlogs", table => {
            table.increments("id").unsigned().notNullable().primary()
            table.specificType("action", "smallint").unsigned().notNullable()
            table.integer("chanid").unsigned().notNullable()
            table.integer("by").unsigned().notNullable()
            table.specificType("dir", "integer ARRAY").unsigned().notNullable()
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.index("by")
            table.index("chanid")
            table.index("dir")
        })
        .createTable("members", table => {
            table.primary(["by", "chanid"])
            table.integer("by").unsigned().notNullable()
            table.integer("chanid").unsigned().notNullable()
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.index("by")
            table.index("chanid")
        })
        .createTable("memberlogs", table => {
            table.increments("id").unsigned().notNullable().primary()
            table.specificType("action", "smallint").unsigned().notNullable()
            table.integer("chanid").unsigned().notNullable()
            table.integer("by").unsigned().notNullable()
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.index("by")
            table.index("chanid")
        })
        .raw(`CREATE OR REPLACE FUNCTION log_chans_members() RETURNS TRIGGER AS $$
            BEGIN
                IF (TG_OP = 'INSERT') THEN
                    INSERT INTO memberlogs ("action", chanid, "by") VALUES (1, NEW.chanid, NEW."by");
                    RETURN NEW;
                ELSIF (TG_OP = 'DELETE') THEN
                    INSERT INTO memberlogs ("action", chanid, "by") VALUES (0, OLD.chanid, OLD."by");
                    RETURN OLD;
                END IF;
            END;
            $$ LANGUAGE plpgsql;`)
        .raw(`CREATE TRIGGER after_members_insert
            AFTER INSERT OR DELETE
            ON members
            FOR EACH ROW
            EXECUTE PROCEDURE log_chans_members();`)
        .createTable("users", table => {
            table.increments("id").unsigned().notNullable().primary()
            table.string("name", 30).notNullable().unique()
            table.string("email", 70).notNullable().unique()
            // table.boolean("emailverified").notNullable().defaultTo(false)
            table.string("fullname", 50).notNullable()
            table.string("key", 60).notNullable()
            table.string("tz", 32)
            table.text("about")
            table.string("st", 160)
            // table.string("pic", 250)
            // table.boolean("gender")
            // table.string("first", 100);
            // table.string("last", 100);
            // table.string("locale", 10);
            // table.boolean("admin").notNullable().defaultTo(false);
            // table.boolean("blocked").notNullable().defaultTo(false);
            // table.boolean("archived").notNullable().defaultTo(false);
            table.timestamp("lastonline").notNullable().defaultTo(knex.fn.now())
            table.timestamp("lastlogin").notNullable().defaultTo(knex.fn.now())
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.index("name")
            table.index("email")
        })
        .createTable("userpushsubs", table => {
            table.text("id").notNullable().primary()
            table.integer("by").unsigned().notNullable()
            table.text("endpoint").notNullable()
            table.text("auth").notNullable()
            table.text("p256dh").notNullable()
            table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
            table.index("by")
        })
        // .createTable("chanchats", table => {
        //     table.primary(["chanid", "chatid"])
        //     table.integer("chanid").unsigned().notNullable()
        //     table.integer("chatid").unsigned().notNullable()
        //     table.timestamp("timestamp").notNullable().defaultTo(knex.fn.now())
        //     table.index("chanid")
        //     table.index("chatid")
        // })
}

exports.down = function (knex) {
    return knex.schema
        .dropTableIfExists("chans")
        .dropTableIfExists("chats")
        .dropTableIfExists("userchans")
        .dropTableIfExists("chatlogs")
        .dropTableIfExists("chanlogs")
        .dropTableIfExists("members")
        .dropTableIfExists("memberlogs")
        .dropTableIfExists("users")
        .dropTableIfExists("userpushsubs")
        // .dropTableIfExists("chanchats")
}

// INSERT INTO openids ("by", "email") VALUES ('1', 'lehuyhoang117@gmail.com');

// INSERT INTO users ("id", "name", "email", "fullname", "key") VALUES
// ('1', 'hoang', 'hoang@hyze.io', 'f', 'hoang', '$2a$10$3J1cJiaCpgrn1FkZWtCEF.qHv.duN.OzHB61roZc/xUM3WuYnLg6O');
