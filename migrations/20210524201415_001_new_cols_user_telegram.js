exports.up = function (knex) {
    return knex.schema.alterTable("users", table => {
        table.bigInteger("tgid").unsigned()
        table.string("tgname", 32)
        table.string("tgfn", 64)
        table.string("tgln", 64)
    })
}

exports.down = function (knex) {
    return knex.schema.alterTable("users", table => {
        table.dropColumn("tgid")
        table.dropColumn("tgname")
        table.dropColumn("tgfn")
        table.dropColumn("tgln")
    })
}
