module.exports = {
    pg: {
        client: "pg",
        connection: {
            host: "127.0.0.1",
            user: "hoang",
            password: "",
            database: "sonic",
        },
        migrations: {
            tableName: "__kex",
        },
    },

    mysql: {
        client: "mysql",
        connection: {
            host: "127.0.0.1",
            user: "root",
            password: "123",
            database: "sonic",
        },
        migrations: {
            tableName: "__kex",
        },
    },

    sqlite: {
        client: "sqlite3",
        connection: {
            filename: "./sonic.sqlite",
        },
        useNullAsDefault: true,
        migrations: {
            tableName: "__kex",
        },
    },

    staging: {
        client: "pg",
        connection: {
            host: "10.15.0.5",
            user: "sonic",
            password: "123",
            database: "sonic",
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "__kex",
        },
    },

    production: {
        client: "pg",
        connection: {
            host: "172.31.32.65",
            user: "sonic",
            password: "123",
            database: "sonic",
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "__kex",
        },
    },
}
