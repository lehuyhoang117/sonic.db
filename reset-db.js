const cfg = require("./knexfile");
const m = require("./migrations/20201116160733_init");

const knex = require('knex')(cfg[process.env.NODE_ENV]);

async function run() {
    await m.down(knex)
    await  m.up(knex)
    console.log("done")
    process.exit()
}
run()